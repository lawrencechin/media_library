<?php

/**
 * Login Controller
 *
**/

class Login extends Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){
		$login_model = $this->loadModel('Login');
		$this->view->render('login/index', false, true);
	}

	function login(){
		$login_model = $this->loadModel('Login');
		//return true or false
		$login_successful = $login_model->login();

		if($login_successful){
			//success! redirect to index/homepage
            Application::app_new_location('index/index');
		}else{
			//boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
		}
	}

	public function demo(){
		$_POST['user_name'] = "test12User";
		$_POST['user_password'] = "test12User";

		$login_model = $this->loadModel('Login');
		//return true or false
		$login_successful = $login_model->login();

		if($login_successful){
			//success! redirect to index/homepage
            Application::app_new_location('index/index');
		}else{
			//boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
		}
	}

    function profile(){
        Auth::handleLogin();
        $login_model = $this->loadModel('Login');
        $movie_model = $this->loadModel('Moviedb');
        $tv_model = $this->loadModel('Tvdb');

        $this->view->totalMov = $movie_model->totalUserMovies();
        $this->view->totalWatchedMov = $movie_model->totalUserWatched();
        $this->view->totalFavMov = $movie_model->totalUserFav();

        $this->view->totalShows = $tv_model->totalUserShows();
        $this->view->totalWatchedShows = $tv_model->totalUserWatched();
        $this->view->totalFavShows = $tv_model->totalUserFav();

        $this->view->render('login/profile');

    }

	function logout(){
		$login_model = $this->loadModel('Login');
		$login_model->logout();
        Application::app_new_location('');
	}

	function loginWithCookie(){
		$login_model = $this->loadModel('Login');
		$login_successful = $login_model->loginWithCookie();

		if($login_successful){
            Application::app_new_location('bookmarks/index');
		}else{
			$login_model->deleteCookie();
            Application::app_new_location('login/index');
		}
	}


	function editUsername(){
		if($this->edit_able){
			Auth::handleLogin();
			$login_model = $this->loadModel('Login');
			$login_model->editUserName();
			Application::app_new_location('login/profile');
		}else{
			$login_model = $this->loadModel('Login');
			$login_model->write_feedback('Disabled in demo');
			Application::app_new_location('login/profile');
		} 
	}

	function editUserEmail(){
		if($this->edit_able){
			Auth::handleLogin();
			$login_model = $this->loadModel('Login');
			$login_model->editUserEmail();
			Application::app_new_location('login/profile');
		}else{
			$login_model = $this->loadModel('Login');
			$login_model->write_feedback('Disabled in demo');
			Application::app_new_location('login/profile');
		}
	}

    function register(){
    	Application::app_new_location('login/index');
    	$login_model = $this->loadModel('Login');
    	$this->view->render('login/register', false, true);
    }

    function register_action(){
    	$login_model = $this->loadModel('Login');
    	$registration_successful = $login_model->registerNewUser();

    	if($registration_successful){
            Application::app_new_location('login/index');
    	}else{
            Application::app_new_location('login/register');
    	}
    }
}






