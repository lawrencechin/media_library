<?php 

/**
* Handles calls to moviedb library
*
**/

class Moviedb extends Controller{
	function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	function index(){
		$this->movie();
	}

	function movie($sortType = NULL, $sort = NULL, $searchTerm = null){
		if($sortType === NULL){
			if(isset($_SESSION['sort_type']) && $_SESSION['sort_type'] !== NULL){
				$sortType = $_SESSION['sort_type'];
			}else{
				$_SESSION['sort_type'] = "movie_title";
				$sortType = $_SESSION['sort_type'];
			}
		}else{
			$_SESSION['sort_type'] = $sortType;
		}

		if($sort === NULL){
			if(isset($_SESSION['sort']) && $_SESSION['sort'] !== NULL){
				$sort = $_SESSION['sort'];
			}else{
				$_SESSION['sort'] = "ASC";
				$sort = $_SESSION['sort'];
			}
		}else{
			$_SESSION['sort'] = $sort;
		}

		$movieDb_model = $this->loadModel('Moviedb');
		$sortTypes = array('movie_title', 'entry_id', 'release_date', 'watched', 'unwatched', 'favourite');

		if(isset($sortType) && in_array($sortType, $sortTypes)){
			$this->view->usrMovies = $movieDb_model->userMovies($sortType, $sort, $searchTerm); 
		}else{
			$this->view->usrMovies = $movieDb_model->userMovies(); 
		}

		$sortTypes = array(
			'movie_title' => 'Title', 
			'release_date' => 'Date', 
			'entry_id' => 'Date Added',  
			'watched' => 'Watched', 
			'unwatched' => 'Unwatched', 
			'favourite' => 'Favourites', 
			'ASC' => 'ASC', 
			'DESC' => 'DESC');

		$this->view->searchTerm = $searchTerm;
		$this->view->sortType = $sortType;
		$this->view->sort = $sort;
		$this->view->sortArr = $sortTypes;
		$this->view->mode = 'moviedb/movie';
		$this->view->render('moviedb/movie');
	}

	function search($searchTerm, $page = 1){
		$searchTerm = str_replace('_', '+', $searchTerm);
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->search = $movieDb_model->search($searchTerm, $page);
		$this->view->pageNumber = $page;
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->searchTerm = str_replace('+', '_', $searchTerm);
		$this->view->mode = 'moviedb/movie';
		$this->view->render('moviedb/search');
	}

	function searchDB($searchTerm){
		if(isset($searchTerm) && !empty($searchTerm)){
			str_replace('_', ' ', $searchTerm);
			str_replace('+', ' ', $searchTerm);
			$this->movie('movie_title', 'ASC', $searchTerm);
		}
	}

	function latest($page = 1){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movies = $movieDb_model->nowPlayingMovies($page);
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->pageNumber = $page;
		$this->view->mode = 'moviedb/latest/';
		$this->view->render('moviedb/latest');
	}

	function popularMovies($page = 1){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movies = $movieDb_model->popularMovies($page);
		$this->view->pageNumber = $page;
		$this->view->mode = 'moviedb/popularMovies/';
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->render('moviedb/latest');
	}

	function upcoming($page = 1){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movies = $movieDb_model->upcomingMovies($page);
		$this->view->pageNumber = $page;
		$this->view->mode = 'moviedb/upcoming/';
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->render('moviedb/latest');
	}

	function movieByPerson($personID){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->personMovies = $movieDb_model->movieByPerson($personID);
		$this->view->person = $movieDb_model->person($personID);
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->mode = 'moviedb/personmov/';
		$this->view->render('moviedb/personmov');
	}

	function movieInfo($movieID){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movieInfo = $movieDb_model->movieDetail($movieID);
		$this->view->movieCredits = $movieDb_model->movieCredits($movieID);
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->render('moviedb/movieinfo', true);
	}

	function posters($idMovie){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->moviePosters = $movieDb_model->moviePoster($idMovie, 'posters');
		$this->view->render('moviedb/posters', true);
	}

	function images($idMovie){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movieImages = $movieDb_model->moviePoster($idMovie, 'backdrops');
		$this->view->render('moviedb/images', true);
	}

	function similar($idMovie){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->similarMovies = $movieDb_model->getSimilarMovies($idMovie);
		$this->view->imgURL = $movieDb_model->getImageURL('w185');
		$this->view->render('moviedb/similar', true);
	}

	function videos($idMovie){
		$movieDb_model = $this->loadModel('Moviedb');
		$this->view->movieVideos = $movieDb_model->movieVideos($idMovie);
		$this->view->render('moviedb/videos', true);
	}

	function addMovie(){
		if($this->edit_able){
			if(isset($_POST['movie_id']) && !empty($_POST['movie_id']) && isset($_POST['movie_title']) && !empty($_POST['movie_title']) && isset($_POST['release_date']) && !empty($_POST['release_date']) && isset($_POST['poster_path'])){

				$movieDb_model = $this->loadModel('Moviedb');
				$result = $movieDb_model->addMovie($_POST['movie_id'],$_POST['movie_title'],$_POST['release_date'],$_POST['poster_path']);

				if($result){
					$this->sendJSON(false, '00');
				}else{
					$this->sendJSON(true, '06');
				}
			}else{
				$this->sendJSON(true, '07');
			}
		}else{
			$this->sendJSON(true, '99');
		}
	}

	function deleteMovie(){
		if($this->edit_able){
			if(isset($_POST['delete_movie']) && is_numeric($_POST['delete_movie']) && isset($_POST['poster_path'])){

				$movieDb_model = $this->loadModel('Moviedb');

				$result = $movieDb_model->removeMovie($_POST['delete_movie'], $_POST['poster_path']);

				if($result){
					$this->sendJSON(false, '05');
				}else{
					$this->sendJSON(true, '07');
				}
			}else{
				$this->sendJSON(true, '07');
			}
		}else{
			$this->sendJSON(false, '05');
		}
	}

	function favMovie(){
		if(isset($_POST['entry_id']) && is_numeric($_POST['entry_id']) && isset($_POST['favourite']) && is_numeric($_POST['favourite'])){

			$movieDb_model = $this->loadModel('Moviedb');

			$result = $movieDb_model->favMovie($_POST['entry_id'], $_POST['favourite']);

			if($result){
				if($_POST['favourite'] == 0){
					$this->sendJSON(false, '02');
				}else{
					$this->sendJSON(false, '01');
				}
			}else{
				$this->sendJSON(true, '07');
			}
		}else{
			$this->sendJSON(true, '07');
		}
	}

	function watched(){
		if(isset($_POST['entry_id']) && is_numeric($_POST['entry_id']) && isset($_POST['watched'])){

			$movieDb_model = $this->loadModel('Moviedb');

			$result = $movieDb_model->watched($_POST['entry_id'], $_POST['watched']);

			if($result){
				if($_POST['watched'] == 0){
					$this->sendJSON(false, '04');
				}else{
					$this->sendJSON(false, '03');
				}
			}else{
				$this->sendJSON(true, '07');
			}
		}else{
			$this->sendJSON(true, '07');
		}
	}

	function autocomplete(){
		$movieDb_model = $this->loadModel('Moviedb');
		echo $movieDb_model->autocomplete();
	}
}