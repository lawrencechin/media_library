<?php 

/**
* Handles calls to tvdb library
*
**/

class Tvdb extends Controller{
	function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	function index(){
		$this->tv();
	}

	function tv($sortType = NULL, $sort = NULL, $searchTerm = null){
		if($sortType === NULL){
			if(isset($_SESSION['sort_type_tv']) && $_SESSION['sort_type_tv'] !== NULL){
				$sortType = $_SESSION['sort_type_tv'];
			}else{
				$_SESSION['sort_type_tv'] = "tv_title";
				$sortType = $_SESSION['sort_type_tv'];
			}
		}else{
			$_SESSION['sort_type_tv'] = $sortType;
		}

		if($sort === NULL){
			if(isset($_SESSION['sort_tv']) && $_SESSION['sort_tv'] !== NULL){
				$sort = $_SESSION['sort_tv'];
			}else{
				$_SESSION['sort_tv'] = "ASC";
				$sort = $_SESSION['sort_tv'];
			}
		}else{
			$_SESSION['sort_tv'] = $sort;
		}

		$tvDb_model = $this->loadModel('Tvdb');

		$sortTypes = array('tv_title', 'entry_id', 'release_date', 'watched', 'unwatched', 'favourite');

		if(isset($sortType) && in_array($sortType, $sortTypes)){
			$this->view->usrtvs = $tvDb_model->userShows($sortType, $sort, $searchTerm); 
		}else{
			$this->view->usrtvs = $tvDb_model->userShows(); 
		}

		$sortTypes = array(
			'tv_title' => 'Title', 
			'release_date' => 'Date', 
			'entry_id' => 'Date Added',  
			'watched' => 'Watched', 
			'unwatched' => 'Unwatched', 
			'favourite' => 'Favourites', 
			'ASC' => 'ASC', 
			'DESC' => 'DESC');

		$this->view->searchTerm = $searchTerm;
		$this->view->sortType = $sortType;
		$this->view->sort = $sort;
		$this->view->sortArr = $sortTypes;
		$this->view->mode = 'tvdb/tv'; 
		$this->view->render('tvdb/tv');
	}

	function searchDB($searchTerm){
		if(isset($searchTerm) && !empty($searchTerm)){
			str_replace('+', ' ', $searchTerm);
			str_replace('_', ' ', $searchTerm);
			$this->tv('tv_title', 'ASC', $searchTerm);
		}
	}

	function thisWeek($page = 1){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tv = $tvDb_model->thisWeek($page);
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->pageNumber = $page;
		$this->view->mode = 'tvdb/thisweek'; 
		$this->view->render('tvdb/popular');
	}

	function popularShows($page = 1){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tv = $tvDb_model->popularShows($page);
		$this->view->pageNumber = $page;
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->mode = 'tvdb/popularShows/'; 
		$this->view->render('tvdb/popular');
	}

	function today($page = 1){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tv = $tvDb_model->today($page);
		$this->view->pageNumber = $page;
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->mode = 'tvdb/today'; 
		$this->view->render('tvdb/popular');
	}

	function seasons($showID, $seasonNum = 1){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->seasons = $tvDb_model->seasons($showID,$seasonNum);
		$this->view->imgURL = $tvDb_model->getImageURL('w300');
		$this->view->render('tvdb/seasons', true);
	}

	function seasonList($seasonNum, $showId){
		$this->view->seasonNum = $seasonNum;
		$this->view->showId = $showId;

		$this->view->render('tvdb/seasonList', true);
	}

	function tvByPerson($personID){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->personTv = $tvDb_model->tvByPerson($personID);
		$this->view->person = $tvDb_model->person($personID);
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->mode = 'tvdb/persontv/';
		$this->view->render('tvdb/persontv');
	}

	function tvInfo($tvID){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tvInfo = $tvDb_model->tvDetail($tvID);
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->tvCredits = $tvDb_model->tvCredits($tvID);
		$this->view->render('tvdb/showinfo', true);
	}

	function posters($idtv){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tvPosters = $tvDb_model->tvPoster($idtv, 'posters');
		$this->view->render('tvdb/posters', true);
	}

	function images($idtv){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tvImages = $tvDb_model->tvPoster($idtv, 'backdrops');
		$this->view->render('tvdb/images', true);
	}

	function similar($idtv){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->similarShows = $tvDb_model->getSimilarShows($idtv);
		$this->view->imgURL = $tvDb_model->getImageURL('w185');
		$this->view->render('tvdb/similar', true);
	}

	function videos($idtv){
		$tvDb_model = $this->loadModel('Tvdb');
		$this->view->tvVideos = $tvDb_model->tvVideos($idtv);
		$this->view->render('tvdb/videos', true);
	}

	function externalLinks($searchTerm){
		$this->view->searchTerm = $searchTerm;
		$this->view->render('tvdb/external', true);
	}

	function addtv(){
		if($this->edit_able){
			if(isset($_POST['tv_id']) && !empty($_POST['tv_id']) && isset($_POST['tv_title']) && !empty($_POST['tv_title']) && isset($_POST['release_date']) && !empty($_POST['release_date']) && isset($_POST['poster_path'])){

				$tvDb_model = $this->loadModel('Tvdb');
				$result = $tvDb_model->addShows($_POST['tv_id'],$_POST['tv_title'],$_POST['release_date'],$_POST['poster_path']);

				if($result){
					$this->sendJSON(false, '10');
				}else{
					$this->sendJSON(true, '16');
				}
			}else{
				$this->sendJSON(true, '17');
			}
		}else{
			$this->sendJSON(true, '99');
		}
	}

	function deletetv(){
		if($this->edit_able){
			if(isset($_POST['delete_tv']) && is_numeric($_POST['delete_tv']) && isset($_POST['poster_path'])){

				$tvDb_model = $this->loadModel('Tvdb');

				$result = $tvDb_model->removeShows($_POST['delete_tv'], $_POST['poster_path']);

				if($result){
					$this->sendJSON(false, '15');
				}else{
					$this->sendJSON(true, '17');
				}
			}else{
				$this->sendJSON(true, '17');
			}
		}else{
			$this->sendJSON(false, '15');
		}	
	}

	function favtv(){
		if(isset($_POST['entry_id']) && is_numeric($_POST['entry_id']) && isset($_POST['favourite']) && is_numeric($_POST['favourite'])){

			$tvDb_model = $this->loadModel('Tvdb');

			$result = $tvDb_model->favShows($_POST['entry_id'], $_POST['favourite']);

			if($result){
				if($_POST['favourite'] == 0){
					$this->sendJSON(false, '12');
				}else{
					$this->sendJSON(false, '11');
				}
			}else{
				$this->sendJSON(true, '17');
			}
		}else{
			$this->sendJSON(true, '17');
		}
	}

	function watched(){
		if(isset($_POST['entry_id']) && is_numeric($_POST['entry_id']) && isset($_POST['watched'])){

			$tvDb_model = $this->loadModel('Tvdb');

			$result = $tvDb_model->watched($_POST['entry_id'], $_POST['watched']);

			if($result){
				if($_POST['watched'] == 0){
					$this->sendJSON(false, '14');
				}else{
					$this->sendJSON(false, '13');
				}
			}else{
				$this->sendJSON(true, '17');
			}
		}else{
			$this->sendJSON(true, '17');
		}
	}
}