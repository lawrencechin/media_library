<?php 

/**
* Handles calls to moviedb library
*
**/

class Index extends Controller{
	function __construct(){
		Auth::handleLogin();
		parent::__construct();
	}

	public function index(){
		$movieDb_model = $this->loadModel('Moviedb');
		$tvDb_model = $this->loadModel('Tvdb');
		$login_model = $this->loadModel('Login');
		$this->view->imgURL = $movieDb_model->getImageURL('w185');

		$this->view->userMovies = $movieDb_model->userMovies('entry_id', 'DESC', null, true);
		$this->view->latestMovies = $movieDb_model->nowPlayingMovies();

		$this->view->userTv = $tvDb_model->userShows('entry_id', 'DESC', null, true);
		$this->view->latestTv = $tvDb_model->today();

		$this->view->totalMov = $movieDb_model->totalUserMovies();
        $this->view->totalWatchedMov = $movieDb_model->totalUserWatched();
        $this->view->totalFavMov = $movieDb_model->totalUserFav();

        $this->view->totalShows = $tvDb_model->totalUserShows();
        $this->view->totalWatchedShows = $tvDb_model->totalUserWatched();
        $this->view->totalFavShows = $tvDb_model->totalUserFav();

		$this->view->mode = 'index/index';
		$this->view->render('index/index');
	}
}

?>