<?php
/**
* Session Class
* creates and manages sessions throughout site
**/

class Session{
	//start session
	public static function init(){
		//start session if no existing session
		if(session_id() == ''){
			session_start();
		}
	}

	//sets specific key for session
	public static function set($key, $value){
		$_SESSION[$key] = $value;
	}

	public static function get($key){
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}
	}

	//deletes session(log off)
	public static function destroy(){
		session_destroy();
	}
}
