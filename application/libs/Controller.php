<?php
/**
* Base level controller. All other controllers extend from this 
*
**/	

class Controller{
	function __construct(){
		//start session
		Session::init();

		//user has 'remember me cookie' then login with cookie
		if(!isset($_SESSION['user_logged_in']) && isset($_COOKIE['rememberme'])){
			Application::app_new_location('login/loginWithCookie');
		}

		//create database connection
		try{
			$this->db = new Database();
		}catch(PDOException $e){
			die('Database connection could not be established');
		}

		//create a view object (that does nothing, but provides the view render() method)
		$this->view = new View();
		$this->edit_able = (int)Session::get('user_edit') === 1 ? true : false; 
	}

	//loads model with specified name
	public function loadModel($name){
		$path = MODELS_PATH . strtolower($name) . '_model.php';
		if(file_exists($path)){
			require MODELS_PATH . strtolower($name) . '_model.php';
			$modelName = $name . 'Model';
			//return model object with database connection
			return new $modelName($this->db);
		}
	}

	protected function sendJSON($error, $statusCode){
		$msg = 'S_'.$statusCode;

		echo json_encode(array(
			'msg' => constant($msg),
			'statusCode' => $statusCode,
			'error' => $error
			));
	}
}