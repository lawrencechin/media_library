<?php

/**
* checks session status
*
**/

class Auth{
	public static function handleLogin(){
		//initialize session
		Session::init();
		//if user not logged in then kill session and redirect to login page
		if(!isset($_SESSION['user_logged_in'])){
			Session::destroy();
			Application::app_new_location('login');
		}
	}
}