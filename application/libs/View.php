<?php
/**
* Class View
**/

class View{
	//renders the specified view
	public function render($filename, $render_without_header_and_footer = false, $render_without_search = false){
		//page without header or footer
		if($render_without_header_and_footer == true){
			require VIEWS_PATH . $filename . '.php';
		}else if($render_without_search){
			require VIEWS_PATH . '_templates/header.php';
			require VIEWS_PATH . $filename . '.php';
			require VIEWS_PATH . '_templates/footer.php';
		}else{
			require VIEWS_PATH . '_templates/header.php';
			require VIEWS_PATH . '_templates/search.php';
			require VIEWS_PATH . $filename . '.php';
			require VIEWS_PATH . '_templates/footer.php';
		}
	}

	//feedback messages
	private function renderFeedBackMessages(){
		//show error/success messages(held in $_SESSION["feedback_postive/negative"])
		require VIEWS_PATH . '_templates/feedback.php';
		//clear session messages after display
		Session::set('feedback_positive', null);
		Session::set('feedback_negative', null);
	}

	private function footerScripts($navController){
		require VIEWS_PATH . $navController.'/_script.php';
	}

	//checks if passed string is currently active controller
	private function checkForActiveController($filename, $navigation_controller){
		$split_filename = explode("/", $filename);
		$active_controller = $split_filename[0];

		if($active_controller == $navigation_controller){
			return true;
		}
		return false;
	}

	//Checks if the passed string is the currently active controller and controller-action.
	private function checkForActiveControllerAndAction($filename, $navigation_controller_and_action){
        $split_filename = explode("/", $filename);
        $active_controller = $split_filename[0];
        $active_action = $split_filename[1];

        $split_filename = explode("/", $navigation_controller_and_action);
        $navigation_controller = $split_filename[0];
        $navigation_action = $split_filename[1];

        if($active_controller == $navigation_controller && $active_action == $navigation_action) {
            return true;
        }
        // default return of not true
        return false;
    }

    private function paginationOLD($url, $pages){
    	echo '</ul>';
		echo '<ul class="pagination top_border">';
		//we don't want to return any more than 20 entries!
		for($i = 1; $i < 21 && $i < $pages+1; $i++){ 
			$finalURL = $url.'/'.$i;
			if($this->pageNumber == $i){ ?>
				<li class="activePage"><a href="<?=$finalURL?>"><?=$i?></a></li> <?php
			}else{ ?>
				<li clas="right_border"><a href="<?=$finalURL?>"><?=$i?></a></li> <?php
			} 
			
		}
		echo '</ul>';
    }

    private function pagination($url, $pages){
    	echo '</ul>';
		echo '<ul class="pagination top_border">';

		if($pages > 10){
			//print first page(always 1)
			if((integer)$this->pageNumber === 1){
				echo '<li class="activePage"><a href="'.$url.'/1">First</a></li>';
			}else{
				echo '<li class="right_border"><a href="'.$url.'/1">First</a></li>';
			}

			$i = $this->pageNumber % 10 === 0 && $pages > 10 || $pages > 10 ? $this->pageNumber - ($this->pageNumber % 10) : 1;

			$i = $i === 0 ? 1 : $i;

			for($i; $i < $pages+1; $i++){
				if($i % 10 === 0 && $i !== 0){
					echo '<li class="right_border"><a href="'.$url.'/'.($i-1).'">--</a></li>';
				}

				if((integer)$this->pageNumber === $i){
					echo '<li class="activePage"><a href="'.$url.'/'.$i.'">'.$i.'</a></li>';
				}else{
					echo '<li class="right_border"><a href="'.$url.'/'.$i.'">'.$i.'</a></li>';
				}
				
				if(($i+1)% 10 === 0){
					echo '<li class="right_border"><a href="'.$url.'/'.($i+1).'">++</a></li>';
					break;
				}
			}

		}else{
			for($i = 1; $i < $pages+1; $i++){ 
				if($this->pageNumber == $i){ ?>
					<li class="activePage"><a href="<?=$url.'/'.$i?>"><?=$i?></a></li> <?php
				}else{ ?>
					<li class="right_border"><a href="<?=$url.'/'.$i?>"><?=$i?></a></li> <?php
				} 
				
			}
		}

		
		echo '</ul>';
    }

}