<?php

class MoviedbModel extends MasterModel{
	const _API_URL_ = "https://api.themoviedb.org/3/";
	private $_apikey = '4e6f1a1a212e4d4e72f318a5d172456e';
	private $_lang = "en&include_image_language=en,null";
	//string url of TMDB images
	private $_imgUrl;
	public function __construct(Database $db){
		parent::__construct($db);
		$conf = $this->getConfig();
		if(empty($conf)){
			echo "Unable to read configuration, verify that the API key is valid";
			exit;
		}
		//set Images URL contain in config
		$this->setImageURL($conf);
	}

	private function _call($action,$text="",$lang=""){
		// # http://api.themoviedb.org/3/movie/11?api_key=XXX
		$lang = (empty($lang))?$this->getLang():$lang;
		$url = self::_API_URL_.$action."?api_key=".$this->getApikey()."&language=".$lang."&".$text;
		$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, 1);

		$results = curl_exec($ch);
		$headers = curl_getinfo($ch);

		$error_number = curl_errno($ch);
		$error_message = curl_error($ch);

		curl_close($ch);
		// header('Content-Type: text/html; charset=iso-8859-1');
		//echo"<pre>";print_r(($results));echo"</pre>";
		$results = json_decode(($results),true);
		return (array) $results;
	}

	public function getConfig(){
		return $this->_call("configuration","");
	}

	public function getLang(){
		return $this->_lang;
	}

	private function getApikey(){
		return $this->_apikey;
	}

	public function setImageURL($config){
		$this->_imgUrl = (string) $config['images']['base_url'];
	}

	public function getImageURL($size = "original"){
		return $this->_imgUrl . $size;
	}


	public function movieVideos($idMovie){
		//get trailers
		$trailer = $this->movieInfo($idMovie,"videos");
		return $trailer;
	}

	public function getSimilarMovies($idMovie){
		//http://api.themoviedb.org/3/movie/similar
		$similar = $this->movieInfo($idMovie, "similar");
		return $similar;
	}

	public function movieDetail($idMovie){
		//get movie details
		return $this->movieInfo($idMovie,"");
	}

	public function movieInfo($idMovie, $option = "", $print = false){
		$option = (empty($option))? "" : "/" . $option;
		$params = "movie/" . $idMovie . $option;
		$movie = $this->_call($params,"");
		return $movie;
	}

	public function movieCredits($idMovie){
		$credits = $this->movieInfo($idMovie, 'credits');
		return $credits;
	}

	public function moviePoster($idMovie, $type){
		//get movie posters
		$posters = $this->movieInfo($idMovie,"images");
		$posters = $posters[$type];
		return $posters;
	}

	public function search($searchTerm, $page = 1){
		$searchTerm = "query=".$searchTerm;
		return $this->_call("search/multi", $searchTerm.'&page='.$page, $this->_lang);

	}

	public function nowPlayingMovies($page = 1){
		return $this->_call('movie/now-playing', 'page='.$page);
	}

	public function popularMovies($page = 1){
		//http://api.themoviedb.org/3/movie/popular
		return $this->_call('movie/popular', 'page='.$page);
	}

	public function upcomingMovies($page = 1){
		return $this->_call('movie/upcoming', 'page='.$page);
	}

	public function movieByPerson($personID, $page=1){
		return $this->_call("person/$personID/movie_credits", 'page='.$page);
	}

	public function person($personID){
		return $this->_call("person/$personID");
	}

	//database functions
	public function userMovies($sortType = 'movie_title', $sort = 'ASC', $searchTerm = null, $limit = null){

		$limit = $limit === null ? '' : 'LIMIT 5 OFFSET 0';

		if($sortType === 'watched'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND watched = 1 ORDER BY movie_title $sort";
			}else{
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND watched = 1 AND movie_title LIKE '%$searchTerm%' ORDER BY movie_title $sort";
			}
			
		}else if($sortType === 'unwatched'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND watched = 0 ORDER BY movie_title $sort";
			}else{
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND watched = 0 AND movie_title LIKE '%$searchTerm%' ORDER BY movie_title $sort";
			}
			
		}else if($sortType === 'favourite'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND favourite = 1 ORDER BY movie_title $sort";
			}else{
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND favourite = 1 AND movie_title LIKE '%$searchTerm%' ORDER BY movie_title $sort";
			}
			
		}else{
			if($searchTerm === null){
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id ORDER BY $sortType $sort $limit";
			}else{
				$sql = "SELECT entry_id, movie_id, movie_title, release_date, poster_path, watched, favourite FROM movies WHERE user_id = :user_id AND movie_title LIKE '%$searchTerm%' ORDER BY $sortType $sort";
			}
			
		}
		
		$dataArr = array('user_id' => $_SESSION['user_id']);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function addMovie($movie_id, $movie_title, $release_date, $poster_path){

		$sql = "SELECT movie_id FROM movies WHERE movie_id = :movie_id AND user_id = :user_id";
		$dataArr = array('movie_id' => $movie_id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if($result){
			return false;
		}
		
		$sql = "INSERT INTO movies(movie_id, user_id, movie_title, release_date, poster_path) VALUES(:movie_id, :user_id, :movie_title, :release_date, :poster_path);";

		$dataArr = array(
			'movie_id' => $movie_id,
			'user_id' => $_SESSION['user_id'],
			'movie_title' => $movie_title,
			'release_date' => $release_date,
			'poster_path' => $poster_path
			);
		
		$result = $this->commitDb($sql, $dataArr, false);
		//grab poster and stick it in the content folder
		$img = file_get_contents("https://image.tmdb.org/t/p/w185".$poster_path);
		if(!empty($img) && $result){
			file_put_contents(NON_HTTP_PATH.CONTENT_DIR.'posters'.$poster_path, $img);
		}
		return $result;
	}

	public function removeMovie($movie_id, $posterPath){
		$sql = "DELETE FROM movies WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'entry_id' => $movie_id,
			'user_id' => $_SESSION['user_id']
			);
		//if database command returns true then check for poster and unlink it
		if($this->commitDb($sql, $dataArr, false)){

			if(file_exists(NON_HTTP_PATH.CONTENT_DIR.'posters'.$posterPath)){
				unlink(NON_HTTP_PATH.CONTENT_DIR.'posters'.$posterPath);
			}

			return true;
		}
	}

	public function favMovie($entryId, $fav){
		$sql = "UPDATE movies SET favourite = :favourite WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'favourite' => $fav,
			'user_id' => $_SESSION['user_id'],
			'entry_id' => $entryId
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function watched($entryId, $watched){
		$sql = "UPDATE movies SET watched = :watched WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'watched' => $watched,
			'user_id' => $_SESSION['user_id'],
			'entry_id' => $entryId
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function totalUserMovies(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM movies WHERE user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn(); 
	}

	public function totalUserWatched(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM movies WHERE watched = 1 AND user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn();
	}

	public function totalUserFav(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM movies WHERE favourite = 1 AND user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn();
	}

	public function autocomplete(){
		//get all titles from movies and television
		$sql = "SELECT movie_title FROM movies WHERE user_id = :user_id";
		$sql2 = "SELECT tv_title FROM television WHERE user_id = :user_id";
		$dataArr = array(
			'user_id' => $_SESSION['user_id']
			);
		//gather results
		$resultOne = $this->commitDb($sql, $dataArr, true);
		$resultTwo = $this->commitDb($sql2, $dataArr, true);

		//new array that we will return for autocomplete
		$arr1 = array();
		$arr2 = array();
		//prepare data for correct format

		foreach($resultOne as $k => $v){
			$arr1[$k]['value'] = $v->movie_title;
			$arr1[$k]['data'] = 'Movie';
		}
		foreach($resultTwo as $k => $v){
			$arr2[$k]['value'] = $v->tv_title;
			$arr2[$k]['data'] = 'Tv';
		}
		//return a json encoded array 
		return json_encode($autoComplete = array_merge($arr1, $arr2));
	}
	//database functions

}

?>

