<?php

class TvdbModel extends MasterModel{
	const _API_URL_ = "https://api.themoviedb.org/3/";
	private $_apikey = '4e6f1a1a212e4d4e72f318a5d172456e';
	private $_lang = "en&include_image_language=en,null";
	//string url of TMDB images
	private $_imgUrl;

	public function __construct(Database $db){
		parent::__construct($db);
		$conf = $this->getConfig();
		if(empty($conf)){
			echo "Unable to read configuration, verify that the API key is valid";
			exit;
		}
		//set Images URL contain in config
		$this->setImageURL($conf);
	}

	private function _call($action,$text = "",$lang= ""){
		// # http://api.themoviedb.org/3/movie/11?api_key=XXX
		$lang = (empty($lang))?$this->getLang():$lang;
		$url = self::_API_URL_.$action."?api_key=".$this->getApikey()."&language=".$lang."&".$text;
		$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, 1);

		$results = curl_exec($ch);
		$headers = curl_getinfo($ch);

		$error_number = curl_errno($ch);
		$error_message = curl_error($ch);

		curl_close($ch);
		// header('Content-Type: text/html; charset=iso-8859-1');
		//echo"<pre>";print_r(($results));echo"</pre>";
		$results = json_decode(($results),true);
		return (array) $results;
	}

	public function getConfig(){
		return $this->_call("configuration","");
	}

	public function getLang(){
		return $this->_lang;
	}

	private function getApikey(){
		return $this->_apikey;
	}

	public function setImageURL($config){
		$this->_imgUrl = (string) $config['images']['base_url'];
	}

	public function getImageURL($size = "original"){
		return $this->_imgUrl . $size;
	}


	public function tvVideos($idMovie){
		//get trailers
		$trailer = $this->tvInfo($idMovie,"videos",false);
		return $trailer;
	}

	public function getSimilarShows($idMovie){
		//http://api.themoviedb.org/3/tv/similar
		$similar = $this->tvInfo($idMovie, "similar", false);
		return $similar;
	}

	public function tvDetail($idMovie){
		//get movie details
		return $this->tvInfo($idMovie,"",false);
	}

	public function tvInfo($idMovie, $option = "", $print = false){
		$option = (empty($option))? "" : "/" . $option;
		$params = "tv/" . $idMovie . $option;
		$tv = $this->_call($params,"");
		return $tv;
	}

	public function tvCredits($idMovie){
		$credits = $this->tvInfo($idMovie, 'credits');
		return $credits;
	}

	public function tvPoster($idMovie, $type){
		//get movie posters
		$posters = $this->tvInfo($idMovie,"images",false);
		$posters = $posters[$type];
		return $posters;
	}

	public function thisWeek($page = 1){
		//shows airing within 7 days
		return $this->_call('tv/on_the_air', 'page='.$page);
	}

	public function popularShows($page = 1){
		//http://api.themoviedb.org/3/movie/popular
		return $this->_call('tv/popular', 'page='.$page);
	}

	public function today($page = 1){
		//shows airing today
		return $this->_call('tv/airing_today', 'page='.$page);
	}

	public function seasons($showID, $seasonNum = 1){
		return $this->_call("tv/$showID/season/$seasonNum", '');
	}

	public function tvByPerson($personID, $page=1){
		return $this->_call("person/$personID/tv_credits", 'page='.$page);
	}

	public function person($personID){
		return $this->_call("person/$personID");
	}

	//database functions
	public function userShows($sortType = 'tv_title', $sort = 'ASC', $searchTerm = null, $limit = null){

		$limit = $limit === null ? '' : 'LIMIT 5 OFFSET 0';

		if($sortType === 'watched'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND watched = 1 ORDER BY tv_title $sort";
			}else{
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND watched = 1 AND tv_title LIKE '%$searchTerm%' ORDER BY tv_title $sort";
			}
			
		}else if($sortType === 'unwatched'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND watched = 0 ORDER BY tv_title $sort";
			}else{
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND watched = 0 AND tv_title LIKE '%$searchTerm%' ORDER BY tv_title $sort";
			}
			
		}else if($sortType === 'favourite'){
			if($searchTerm === null){
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND favourite = 1 ORDER BY tv_title $sort";
			}else{
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND favourite = 1 AND tv_title LIKE '%$searchTerm%' ORDER BY tv_title $sort";	
			}
			
		}else{
			if($searchTerm === null){
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id ORDER BY $sortType $sort $limit";
			}else{
				$sql = "SELECT entry_id, tv_id, tv_title, release_date, poster_path, watched, favourite FROM television WHERE user_id = :user_id AND tv_title LIKE '%$searchTerm%' ORDER BY $sortType $sort";
			}
			
		}
		
		$dataArr = array('user_id' => $_SESSION['user_id']);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function addShows($tv_id, $tv_title, $release_date, $poster_path){

		$sql = "SELECT tv_id FROM television WHERE tv_id = :tv_id AND user_id = :user_id";
		$dataArr = array('tv_id' => $tv_id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if($result){
			return false;
		}
		
		$sql = "INSERT INTO television(tv_id, user_id, tv_title, release_date, poster_path) VALUES(:tv_id, :user_id, :tv_title, :release_date, :poster_path);";

		$dataArr = array(
			'tv_id' => $tv_id,
			'user_id' => $_SESSION['user_id'],
			'tv_title' => $tv_title,
			'release_date' => $release_date,
			'poster_path' => $poster_path
			);
		
		$result = $this->commitDb($sql, $dataArr, false);
		//grab poster and stick it in the content folder
		$img = file_get_contents("https://image.tmdb.org/t/p/w185".$poster_path);
		if(!empty($img) && $result){
			file_put_contents(NON_HTTP_PATH.CONTENT_DIR.'posters'.$poster_path, $img);
		}
		return $result;
	}

	public function removeShows($tv_id, $posterPath){
		$sql = "DELETE FROM television WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'entry_id' => $tv_id,
			'user_id' => $_SESSION['user_id']
			);
		//if database command returns true then check for poster and unlink it
		if($this->commitDb($sql, $dataArr, false)){
			if(file_exists(NON_HTTP_PATH.CONTENT_DIR.'posters'.$posterPath)){
				unlink(NON_HTTP_PATH.CONTENT_DIR.'posters'.$posterPath);
			}

			return true;
		}
	}

	public function favShows($entryId, $fav){
		$sql = "UPDATE television SET favourite = :favourite WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'favourite' => $fav,
			'entry_id' => $entryId,
			'user_id' => $_SESSION['user_id']
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function watched($entryId, $watched){
		$sql = "UPDATE television SET watched = :watched WHERE entry_id = :entry_id AND user_id = :user_id";
		$dataArr = array(
			'watched' => $watched,
			'user_id' => $_SESSION['user_id'],
			'entry_id' => $entryId
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function totalUserShows(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM television WHERE user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn();
	}

	public function totalUserWatched(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM television WHERE watched = 1 AND user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn();
	}

	public function totalUserFav(){
		$user_id = $_SESSION['user_id'];
		$sql = "SELECT Count(*) FROM television WHERE favourite = 1 AND user_id = $user_id;";
		return $this->db->query($sql)->fetchColumn();
	}
	//database functions

}

?>

