<nav class="secondary_header">
	<ul>
		<li <?php if($this->checkForActiveControllerAndAction($filename, 'moviedb/movie')){echo 'class="active"';}?>
		><span><a href="<?=URL.'moviedb/movie'?>">My Movies</a></span></li>

		<li><span <?php if($this->mode == 'moviedb/latest/'){echo 'class="active"';}?>
		><a href="<?=URL.'moviedb/latest'?>">Latest</a></span></li>

		<li <?php if($this->mode == 'moviedb/upcoming/'){echo 'class="active"';}?>
		><span><a href="<?=URL.'moviedb/upcoming'?>">Upcoming</a></span>
		</li>

		<li <?php if($this->mode == 'moviedb/popularMovies/'){echo 'class="active"';}?>
		><span><a href="<?=URL.'moviedb/popularMovies'?>">Popular</a></span>
		</li>

	</ul>
</nav>