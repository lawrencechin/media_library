<section class="search_results">
<?php if(!empty($this->upcomingMovies)){
	echo '<ul class="media_list">';
	foreach($this->upcomingMovies['results'] as $results){ ?>

		<li>
		
			<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$results['id']?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/lazyload_poster.svg'?>" data-src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$results['poster_path'];?>" width="185px" height="278px"></a>
			<a class="title" href="<?=URL.'moviedb/movieinfo/'.$results['id']?>"><sub>Title: </sub><span><?=$results['title']?></span></a>
			<sub>Release Date: </sub><span class="date"><?=$results['release_date']?></span>

			<button class="add_movie"></button>

			<form method="post" name="add_movie" class="add_movie" action="<?=URL.'moviedb/addMovie'?>" >
				<input type="hidden" name="movie_id" value="<?=$results['id']?>">
				<input type="hidden" name="movie_title" value="<?=$results['title']?>">
				<input type="hidden" name="release_date" value="<?=$results['release_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>Try another search</p></div>";
}
$url = URL.'moviedb/upcoming/'; 
$this->pagination($url, $this->upcomingMovies['total_pages']);
?>
</section>