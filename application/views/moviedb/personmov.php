<section class="search_results">
<?php if(!empty($this->person)){ ?>
	<div class="person_info_cont">
		<div class="profile_pic"><img width="185px" src="<?=$this->person['profile_path'] == ''? URL.PUBLIC_IMAGES.'btns/spacer.png' : $this->imgURL.$this->person['profile_path']?>"></div>
		<ul class="person_info">
			<li><sub>Name:  </sub><?=$this->person['name'] == ''? 'No name' : $this->person['name']?></li>
			<li><sub>Birthday:  </sub><?=$this->person['birthday'] == ''? 'Unknown' : $this->person['birthday']?></li>
			<li><sub>Birthplace:  </sub><?=$this->person['place_of_birth'] == ''? 'Unknown' : $this->person['place_of_birth']?></li>
			<li><sub>Media:  </sub><a href="<?=URL.'moviedb/movieByPerson/'.$this->person['id']?>" class="active">Movies</a></li>
			<li><sub>Media:  </sub><a href="<?=URL.'tvdb/tvByPerson/'.$this->person['id']?>">Television</a></li>
			
		</ul>
		<div class="bio">
			<p><?=$this->person['biography'] == ''? 'No biography': $this->person['biography']?></p>
		</div>
		<?php if($this->person['biography'] != ''){
			echo '<button data-more="1" onclick="navigation.moreBio(this)">More</button>';
		}?>
		
	</div>

<?php }

if(!empty($this->personMovies)){
	echo '<ul class="media_list">';
	if(!empty($this->personMovies['cast'])){
		foreach($this->personMovies['cast'] as $results){ ?>

			<li>
			
				<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$results['id']?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/default_person.svg'?>" data-src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$results['poster_path'];?>" width="185px" height="278px"></a>
				<button class="add_movie"></button>
				<a class="title" href="<?=URL.'moviedb/movieinfo/'.$results['id']?>"><sub>Title: </sub><span><?=$results['title']?></span></a>
				<sub>Release Date: </sub><span class="date"><?=$results['release_date']?></span>


				<form method="post" name="add_movie" class="add_movie" action="<?=URL.'moviedb/addMovie'?>" >
					<input type="hidden" name="movie_id" value="<?=$results['id']?>">
					<input type="hidden" name="movie_title" value="<?=$results['title']?>">
					<input type="hidden" name="release_date" value="<?=$results['release_date']?>">
					<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
					<button type="submit">Submit</button>
				</form>

			</li>

		<?php }
		echo '</ul>';
	}else{
		echo "<div class='no_results'><h2>No results</h2><p>This person has no <i>movie</i> credits.</p></div>";
	}
	
}else{
	echo "<div class='no_results'><h2>No results</h2><p>Try another search</p></div>";
}
?>
</section>