<section class="search_results">

<?php if(!empty($this->search)){
	echo '<ul class="media_list">';
	foreach($this->search['results'] as $results){
		if($results['media_type'] != 'person'){ ?>

		<li>
		
			<a class="poster" href="<?php echo $results['media_type'] == 'movie'? URL.'moviedb/movieinfo/'.$results['id'] : URL.'tvdb/tvinfo/'.$results['id'];?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/lazyload_poster.svg'?>" data-src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$results['poster_path'];?>" width="185px" height="278px"></a>

			<?php if($results['media_type'] == 'movie'){
				echo '<button class="add_movie"></button>';
			}else{
				echo '<button class="add_tv"></button>' ;
			}?>

			<a class="title" href="<?php echo $results['media_type'] == 'movie'? URL.'moviedb/movieinfo/'.$results['id'] : URL.'tvdb/tvinfo/'.$results['id'];?>"><sub>Title: </sub><span><?php echo $results['media_type'] == 'movie'? $results['title'] : $results['name'];?></span></a>
			<sub>Release Date: </sub><span class="date"><?php echo $results['media_type'] == 'movie'? $results['release_date'] : $results['first_air_date'];?></span>

			<?php if($results['media_type'] == 'movie'){ ?>

			<form method="post" name="add_movie" class="add_movie" action="<?=URL.'moviedb/addMovie'?>" >
				<input type="hidden" name="movie_id" value="<?=$results['id']?>">
				<input type="hidden" name="movie_title" value="<?=$results['title']?>">
				<input type="hidden" name="release_date" value="<?=$results['release_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

			<?php }else{ ?>

			<form method="post" name="add_tv" class="add_tv" action="<?=URL.'tvdb/addtv'?>" >
				<input type="hidden" name="tv_id" value="<?=$results['id']?>">
				<input type="hidden" name="tv_title" value="<?=$results['name']?>">
				<input type="hidden" name="release_date" value="<?=$results['first_air_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

			<?php } ?>

		</li>

	<?php }else{ ?>

			<li>
				<a class="poster noModal" href="<?=URL.'moviedb/movieByPerson/'.$results['id']?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/default_person.svg'?>" data-src="<?php echo $results['profile_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_person.svg' : $this->imgURL.$results['profile_path'];?>" width="185px" height="278px"></a>
				<a class="title noModal" href="<?=URL.'moviedb/movieByPerson/'.$results['id']?>"><sub>Title: </sub><span><?=$results['name']?></span></a>
			</li>

		<?php }
	}
	echo '</ul>';
	//returns an array but with no results
	if($this->search['total_results'] === 0){
		echo "<div class='no_results'><h2>No results</h2><p>Try another search</p></div>";
	}
}else{
	echo "<div class='no_results'><h2>No results</h2><p>Try another search</p></div>";
}
$url = URL.'moviedb/search/'.$this->searchTerm; 

if($this->search['total_results'] === 0){
	//don't run pagination
}else{
	$this->pagination($url, $this->search['total_pages']);
}

?>
<script>
var input = document.getElementById('search_all'),
str = "<?=$this->searchTerm?>";
input.value = str.replace(/[_]/g,' ');
</script>

</section>