<?php if(count($this->similarMovies['results']) > 0){
	echo '<ul class="media_list similar">';
	$url = 'https://image.tmdb.org/t/p/';
	$thumb =  'w150';
	foreach($this->similarMovies['results'] as $results){ ?>

		<li>
		
			<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$results['id']?>"><img src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $url.$thumb.$results['poster_path'];?>" width="150px" height="220px"></a>
			<button class="add_movie"></button>
			<a class="title"><sub>Title: </sub><span><?=$results['title']?></span></a>

			<form method="post" name="add_movie" class="add_movie" action="<?=URL.'moviedb/addMovie'?>" >
				<input type="hidden" name="movie_id" value="<?=$results['id']?>">
				<input type="hidden" name="movie_title" value="<?=$results['title']?>">
				<input type="hidden" name="release_date" value="<?=$results['release_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>No similar movies i'm afraid.</p></div>";
}
?>

<script>forms.formBtn($('.similar'));
navigation.load($('.similar'));</script>