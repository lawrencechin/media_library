<section class="movies">
<?php 

if(!empty($this->usrMovies)){ ?>
	<header class="sortingOptions bottom_border">
		<span class="active">Sort by:</span>

		<ul class="firstSortingCriteria">
		<?php foreach($this->sortArr as $k => $v){ 
			if($k == 'ASC' || $k == 'DESC'){ 
				//do nothing 
			}else{ ?>

				<li <?php echo $k == $this->sortType ? 'class="search_active"' : '' ?>><a href="<?=URL.'moviedb/movie/'.$k.'/'?>"><span><?=$v?></span></a></li>
			<?php } 
			
		} ?>
	
		</ul>

		<ul class="secondSortingCriteria">
			<?php foreach($this->sortArr as $k => $v){ 
			if($k == 'ASC' || $k == 'DESC'){ ?>
				<li <?php echo $k == $this->sort ? 'class="search_active"' : '' ?>><a href="<?=$k.'/'.$this->searchTerm?>"><span><?=$v?></span></a></li>
				<?php }else{ //do nothing
				} 
			} ?>
		</ul>

		<button class="go" onclick="sort.submitSort(this)"></button>
	</header>
	<?php echo '<ul class="media_list myMovies" id="myMovies">';
	foreach($this->usrMovies as $results){ ?>
		<li>

			<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$results->movie_id?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/lazyload_poster.svg'?>" data-src="<?php echo $results->poster_path == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : URL.CONTENT_DIR.'posters'.$results->poster_path;?>" width="185px" height="278px"></a>
			<a class="title" href="<?=URL.'moviedb/movieinfo/'.$results->movie_id?>"><sub>Title: </sub><span><?=$results->movie_title?></span></a>
			<p class="padding bottom_border"><sub>Release Date: </sub><span class="date"><?=$results->release_date?></span></p>

			<?=(integer)$results->favourite === 0 ? '<button class="fav_movie" title="Mark as Favourite"></button>' : '<button class="fav_movie activeFavOrWatched" title="Unmark as Favourite"></button>'?>	

			<?=(integer)$results->watched === 0 ? '<button class="watched" title="Mark as Watched"></button>' : '<button class="watched activeFavOrWatched" title="Mark as Unwatched"></button>'?>	
			
			<button class="delete_movie" title="Delete Movie"></button>

			<form method="post" name="fav_movie" class="fav_movie" action="<?=URL.'moviedb/favMovie'?>">
				<input type="hidden" name="entry_id" value="<?=$results->entry_id?>">
				<?=(integer)$results->favourite === 0 ? '<input type="hidden" name="favourite" value="1">' : '<input type="hidden" name="favourite" value="0">'?>	
				<button type="submit">Submit</button>
			</form>

			<form method="post" name="watched" class="watched" action="<?=URL.'moviedb/watched'?>">
				<input type="hidden" name="entry_id" value="<?=$results->entry_id?>">
				<?=(integer)$results->watched === 0 ? '<input type="hidden" name="watched" value="1">' : '<input type="hidden" name="watched" value="0">'?>	
				
				<button type="submit">Submit</button>
			</form>

			<form method="post" name="delete_movie" class="delete_movie" action="<?=URL.'moviedb/deleteMovie'?>">
				<input type="hidden" name="delete_movie" value="<?=$results->entry_id?>">
				<input type="hidden" name='poster_path' value="<?=$results->poster_path?>">
				<button type="submit">Submit</button>
			</form>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>You haven't saved any movies. <strong><i>Add some!</i></strong></p></div>";
}
?>

<script>
var input = document.getElementById('search_all'),
str = "<?=$this->searchTerm?>";
input.value = str.replace(/[_]/g,' ');
</script>
</section>