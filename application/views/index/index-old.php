<section class="main_index">

<ul id="user_movies" class="user_movies">
	<li class="section_heading"><h2><a class="noModal" href="<?=URL.'moviedb/movie'?>">Recently Added Movies</a></h2><span id="user_movies_heading" class="heading_bullet umh"></span></li> 
<?php if(!empty($this->userMovies)){ ?>
	<li class="statistics">
		<ul class="movie_stats">
			<li><a class="noModal" href="<?=URL.'moviedb/movie'?>">Total</a> Movies:</li>
			<li><span><?=$this->totalMov?></span></li>
			<li><a class="noModal" href="<?=URL.'moviedb/movie/watched/ASC/'?>">Watched</a> Movies:</li>
			<li><span><?=$this->totalWatchedMov?></span></li>
			<li><a class="noModal" href="<?=URL.'moviedb/movie/favourite/ASC/'?>">Favourite</a> Movies:</li>
			<li><span><?=$this->totalFavMov?></span></li>
		</ul>
	</li>
	<?php foreach($this->userMovies as $usrMov){ ?>
	<li>
		<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$usrMov->movie_id?>"><img src="<?php echo $usrMov->poster_path == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : URL.CONTENT_DIR.'posters'.$usrMov->poster_path;?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'moviedb/movieinfo/'.$usrMov->movie_id?>"><sub>Title: </sub><span><?=$usrMov->movie_title?></span></a>
	</li>
	<?php }
}else{
	echo "<li><div class='no_results'><h2>No results</h2><p>You haven't saved any movies. <strong><i>Add some!</i></strong></p></div></li>";
}?>
</ul>

<ul id="latest_movies" class="latest_movies">
	<li class="section_heading"><h2><a class="noModal" href="<?=URL.'moviedb/movie/latest'?>">Recently Released Movies</a></h2><span id="latest_movies_heading" class="heading_bullet lmh"></span></li> 
<?php if(!empty($this->latestMovies)){ 
	for($i = 0; $i < 6; $i++){ ?>
	<li>
		<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$this->latestMovies['results'][$i]['id']?>"><img src="<?php echo $this->latestMovies['results'][$i]['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$this->latestMovies['results'][$i]['poster_path'];?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'moviedb/movieinfo/'.$this->latestMovies['results'][$i]['id']?>"><sub>Title: </sub><span><?=$this->latestMovies['results'][$i]['title']?></span></a>
	</li>
	<?php }
}?>
</ul>

<ul id="user_tv" class="user_tv">
	<li class="section_heading"><h2><a class="noModal" href="<?=URL.'tvdb/tv'?>">Recently Added Television</a></h2><span id="user_tv_heading" class="heading_bullet uth"></span></li> 
<?php if(!empty($this->userTv)){ ?>
	<li class="statistics">
		<ul class="movie_stats">
			<li><a class="noModal" href="<?=URL.'tvdb/tv'?>">Total</a> Shows:</li>
			<li><span><?=$this->totalShows?></span></li>
			<li><a class="noModal" href="<?=URL.'tvdb/tv/watched/ASC/'?>">Watched</a> Shows:</li>
			<li><span><?=$this->totalWatchedShows?></span></li>
			<li><a class="noModal" href="<?=URL.'tvdb/tv/favourite/ASC/'?>">Favourite</a> Shows:</li>
			<li><span><?=$this->totalFavShows?></span></li>
		</ul>
	</li>
	<?php foreach($this->userTv as $usrTv){ ?>
	<li>
		<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$usrTv->tv_id?>"><img src="<?php echo $usrTv->poster_path == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : URL.CONTENT_DIR.'posters'.$usrTv->poster_path;?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'tvdb/tvinfo/'.$usrTv->tv_id?>"><sub>Title: </sub><span><?=$usrTv->tv_title?></span></a>
	</li>
	<?php }
}?>
</ul>

<ul id="latest_tv" class="latest_tv">
	<li class="section_heading"><h2><a class="noModal" href="<?=URL.'tvdb/thisweek'?>">Recently Aired Television</h2><span id="latest_tv_heading" class="heading_bullet lth"></span></li> 
<?php if(!empty($this->latestTv)){
	for($i = 0; $i < 6; $i++){ ?>
	<li>
		<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$this->latestTv['results'][$i]['id']?>"><img src="<?php echo $this->latestTv['results'][$i]['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$this->latestTv['results'][$i]['poster_path'];?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'tvdb/tvinfo/'.$this->latestTv['results'][$i]['id']?>"><sub>Title: </sub><span><?=$this->latestTv['results'][$i]['name']?></span></a>
	</li>
	<?php }
}?>
</ul>

</section>
