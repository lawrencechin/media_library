<nav class="secondary_header">
	<ul>
		<li><a href="<?=URL.'login/profile'?>">Profile</a></li>

		<li><span class="heading_bullet smaller umh"></span><a href="#user_movies">Added Movies</a></li>

		<li><span class="heading_bullet smaller lmh"></span><a href="#latest_movies">Recently Released</a></li>

		<li><span class="heading_bullet smaller uth"></span><a href="#user_tv">Added Television</a></li>

		<li><span class="heading_bullet smaller lth"></span><a href="#latest_tv">Recently Aired</a></li>

	</ul>
</nav>