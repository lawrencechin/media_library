<section class="main_index">

<ul id="user_movies" class="user_movies">
	<li id="user_movies" class="section_heading"><h2><a class="noModal" href="<?=URL.'moviedb/movie/entry_id/DESC/'?>">Recently Added Movies</a></h2><span id="user_movies_heading" class="heading_bullet umh"></span></li> 
<?php if(!empty($this->userMovies)){ ?>
	<li class="statistics">
		<ul class="movie_stats">
			<li><a class="noModal" href="<?=URL.'moviedb/movie'?>">Total</a> Movies:</li>
			<li><span><?=$this->totalMov?></span></li>
			<li><a class="noModal" href="<?=URL.'moviedb/movie/watched/ASC/'?>">Watched</a> Movies:</li>
			<li><span><?=$this->totalWatchedMov?></span></li>
			<li><a class="noModal" href="<?=URL.'moviedb/movie/favourite/ASC/'?>">Favourite</a> Movies:</li>
			<li><span><?=$this->totalFavMov?></span></li>
		</ul>
	</li>
	<?php foreach($this->userMovies as $usrMov){ ?>
	<li>
		<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$usrMov->movie_id?>"><img src="<?php echo $usrMov->poster_path == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : URL.CONTENT_DIR.'posters'.$usrMov->poster_path;?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'moviedb/movieinfo/'.$usrMov->movie_id?>"><sub>Title: </sub><span><?=$usrMov->movie_title?></span></a>
	</li>
	<?php }
}else{
	echo "<li class='no_res'><div class='no_results'><h2>No results</h2><p>You haven't saved any movies. <strong><i>Add some!</i></strong></p></div></li>";
}	
	//news placeholders ?>
	<li id="news_0" class="news_content"></li>
	<li id="news_1" class="news_content"></li>
	<li id="news_2" class="news_content"></li>

	<li id="latest_movies" class="section_heading"><h2><a class="noModal" href="<?=URL.'moviedb/latest'?>">Recently Released Movies</a></h2><span id="latest_movies_heading" class="heading_bullet lmh"></span></li> 
<?php if(!empty($this->latestMovies)){ 
	for($i = 0; $i < 6; $i++){ ?>
	<li>
		<a class="poster" href="<?=URL.'moviedb/movieinfo/'.$this->latestMovies['results'][$i]['id']?>"><img src="<?php echo $this->latestMovies['results'][$i]['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$this->latestMovies['results'][$i]['poster_path'];?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'moviedb/movieinfo/'.$this->latestMovies['results'][$i]['id']?>"><sub>Title: </sub><span><?=$this->latestMovies['results'][$i]['title']?></span></a>
	</li>
	<?php }
}?>
	<li id="news_3" class="news_content "></li>
	<li id="news_4" class="news_content"></li>
	<li id="news_5" class="news_content "></li>


	<li id="user_tv" class="section_heading"><h2><a class="noModal" href="<?=URL.'tvdb/tv/entry_id/DESC/'?>">Recently Added Television</a></h2><span id="user_tv_heading" class="heading_bullet uth"></span></li> 

<?php if(!empty($this->userTv)){ ?>
	<li class="statistics">
		<ul class="movie_stats">
			<li><a class="noModal" href="<?=URL.'tvdb/tv'?>">Total</a> Shows:</li>
			<li><span><?=$this->totalShows?></span></li>
			<li><a class="noModal" href="<?=URL.'tvdb/tv/watched/ASC/'?>">Watched</a> Shows:</li>
			<li><span><?=$this->totalWatchedShows?></span></li>
			<li><a class="noModal" href="<?=URL.'tvdb/tv/favourite/ASC/'?>">Favourite</a> Shows:</li>
			<li><span><?=$this->totalFavShows?></span></li>
		</ul>
	</li>
	<?php foreach($this->userTv as $usrTv){ ?>
	<li>
		<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$usrTv->tv_id?>"><img src="<?php echo $usrTv->poster_path == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : URL.CONTENT_DIR.'posters'.$usrTv->poster_path;?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'tvdb/tvinfo/'.$usrTv->tv_id?>"><sub>Title: </sub><span><?=$usrTv->tv_title?></span></a>
	</li>
	<?php }
}else{
	echo "<li class='no_res'><div class='no_results'><h2>No results</h2><p>You haven't saved any shows. <strong><i>Add some!</i></strong></p></div></li>";
}?>

	<li id="news_6" class="news_content "></li>
	<li id="news_7" class="news_content"></li>
	<li id="news_8" class="news_content "></li>

	<li id="latest_tv" class="section_heading"><h2><a class="noModal" href="<?=URL.'tvdb/thisweek'?>">Recently Aired Television</a></h2><span id="latest_tv_heading" class="heading_bullet lth"></span></li> 
<?php if(!empty($this->latestTv)){
	for($i = 0; $i < 6; $i++){ ?>
	<li>
		<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$this->latestTv['results'][$i]['id']?>"><img src="<?php echo $this->latestTv['results'][$i]['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$this->latestTv['results'][$i]['poster_path'];?>" width="185px" height="278px"></a>
		<a class="title" href="<?=URL.'tvdb/tvinfo/'.$this->latestTv['results'][$i]['id']?>"><sub>Title: </sub><span><?=$this->latestTv['results'][$i]['name']?></span></a>
	</li>
	<?php }
}?>
	<li id="news_9" class="news_content "></li>
	<li id="news_10" class="news_content"></li>
	<li id="news_11" class="news_content "></li>
</ul>
<br>
<p><i>Movie</i> and <i>Television</i> news supplied by <strong><a class="noModal blue_border" target="_blank" href="http://screenrant.com">Screen Rant</a></strong></p>
</section>
