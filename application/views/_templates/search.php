<div class="search_all bottom_border">
<ul class="search_options">
	<li id="smm"><a href="<?=URL.'moviedb/searchDB/'?>"><span><sub>Search: </sub>my movies</span></a></li>
	<li id="sms"><a href="<?=URL.'tvdb/searchDB/'?>"><span><sub>Search: </sub>my shows</span></a></li>
	<li id="sa" class="search_active"><a href="<?=URL.'moviedb/search/'?>"><span><sub>Search: </sub>all</span></a></li>
</ul>

<input id="search_all" class="mousetrap" placeholder="Shortcuts: !+m+m My Movies, !+m+s My Shows & !+s+a Search All">
<button type="submit" id="submit_search_all" onclick="search.submit_search(this);" class="submit_search all"></button>
</div>