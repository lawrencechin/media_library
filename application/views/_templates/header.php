<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MediaLibrary</title>
    <meta name="description" content="Media Library">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- CSS -->
    <link rel="stylesheet" href="<?=URL?>public/css/screen.min.css" />
    <link rel="shortcut icon" href="<?=URL.'public/images/favicons/favicon.ico'?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=URL.'public/images/favicons/apple-touch-icon-57x57.png'?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=URL.'public/images/favicons/apple-touch-icon-114x114.png'?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=URL.'public/images/favicons/apple-touch-icon-72x72.png'?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=URL.'public/images/favicons/apple-touch-icon-144x144.png'?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=URL.'public/images/favicons/apple-touch-icon-60x60.png'?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=URL.'public/images/favicons/apple-touch-icon-120x120.png'?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=URL.'public/images/favicons/apple-touch-icon-76x76.png'?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=URL.'public/images/favicons/apple-touch-icon-152x152.png'?>">
    <meta name="apple-mobile-web-app-title" content="Media Library">
    <link rel="icon" type="image/png" href="<?=URL.'public/images/favicons/favicon-196x196.png'?>" sizes="196x196">
    <link rel="icon" type="image/png" href="<?=URL.'public/images/favicons/favicon-160x160.png'?>" sizes="160x160">
    <link rel="icon" type="image/png" href="<?=URL.'public/images/favicons/favicon-96x96.png'?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=URL.'public/images/favicons/favicon-16x16.png'?>" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=URL.'public/images/favicons/favicon-32x32.png'?>" sizes="32x32">
    <meta name="msapplication-TileColor" content="#d8d8d8">
    <meta name="msapplication-TileImage" content="<?=URL.'public/images/favicons/mstile-144x144.png'?>">
    <meta name="msapplication-config" content="<?=URL.'public/images/favicons/browserconfig.xml'?>">
    <meta name="application-name" content="Media Library">
</head>
<body>
    <header class="header" id="header">
        <?php if(Session::get('user_logged_in')){?>
        <nav class="main_nav">
            <ul class="clearfix">
                <li class="left <?php if($this->checkForActiveController($filename, 'login')){echo 'active';}?>">
                <span class="header_user_name"><a href="<?=URL.'index/index'?>"><?=Session::get('user_name') ?></a></span></li>
                <li class="right"><span class="logout"><a href="<?=URL?>login/logout">Logout</a></span></li>
                 
                <li class="<?php if($this->checkForActiveController($filename, 'moviedb')){echo 'active';}?>
                "><a href="<?=URL?>moviedb/movie">Film</a></li>

                <li class="<?php if($this->checkForActiveController($filename, 'tvdb')){echo 'active';}?>
                "><a href="<?=URL?>tvdb/tv">Television</a></li>
            </ul>  
        </nav>
        <?php
            if($this->checkForActiveController($filename, 'moviedb')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'moviedb/_header.php';
            }else if($this->checkForActiveController($filename, 'tvdb')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'tvdb/_header.php';
            }else if($this->checkForActiveController($filename, 'index')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'index/_header.php';
            }
        }?>
    </header>