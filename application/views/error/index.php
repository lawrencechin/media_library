<br>
<div class="no_results">
	<h2>Something Went <i>Wrong</i></h2>
	<p>Perhaps you'd be better off <strong>returning</strong> where you came from</p>
	<p>You could try another link instead.</p>
	<br>
	<hr>
	<p>Whatever you <i>desire</i>.</p>
</div>