<script>

navigation.load($('.media_list'));
forms.formBtn($('.media_list'));

if($('.shows').length){
	sort.sortBy('.firstSortingCriteria');
	sort.sortBy('.secondSortingCriteria');

	$('#myShows').liveFilter('#search_all', 'li',{
	  filterChildSelector: 'a.title'
	});
}

$('.poster > img').unveil(200);

</script>