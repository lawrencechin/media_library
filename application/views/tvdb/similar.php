<?php if(count($this->similarShows['results']) > 0){
	echo '<ul class="media_list similar">';
	$url = 'https://image.tmdb.org/t/p/';
	$thumb =  'w150/';
	foreach($this->similarShows['results'] as $results){ ?>

		<li>
		
			<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$results['id']?>"><img src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $url.$thumb.$results['poster_path'];?>" width="150px" height="220px"></a>
			<button class="add_movie"></button>
			<a class="title"><sub>Title: </sub><span><?=$results['name']?></span></a>

			

			<form method="post" name="add_movie" class="add_movie" action="<?=URL.'tvdb/addTv'?>" >
				<input type="hidden" name="tv_id" value="<?=$results['id']?>">
				<input type="hidden" name="tv_title" value="<?=$results['name']?>">
				<input type="hidden" name="release_date" value="<?=$results['first_air_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>No similar movies i'm afraid.</p></div>";
}
?>

<script>forms.formBtn($('.similar'));
navigation.load($('.similar'));</script>