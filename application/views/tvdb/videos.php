<?php

if(!empty($this->tvVideos)){
	if(empty($this->tvVideos['results'] )){
		echo "<div class='no_results'><h2>No results</h2><p>There are not any videos.</p></div>";
	}else{
		echo '<ul class="media_list movieVideos">';

		foreach($this->tvVideos['results'] as $results){ ?>
			<li>
				<iframe width="300" height="168.73" src="//www.youtube-nocookie.com/embed/<?=$results['key']?>" frameborder="0" allowfullscreen></iframe>
				<sub><?=$results['type']?>: </sub><span><?=$results['name']?></span>
			</li>
		<?php }

		echo '</ul>';
	}
	
}else{
	echo "<div class='no_results'><h2>No results</h2><p>There are not any videos.</p></div>";
}

?>