<?php

if(!empty($this->tvImages)){
	$url = 'https://image.tmdb.org/t/p/';
	$thumb =  'w300';
	$original = 'original';
	echo '<ul class="media_list movieImages">';

	foreach($this->tvImages as $results){ ?>

		<li><a class="poster" href="<?=$url.$original.$results['file_path']?>"><img src="<?=$url.$thumb.$results['file_path']?>" width="300px" height="169px"></a></li>

	<?php }

	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>No images monsieur</p></div>";
}

?>