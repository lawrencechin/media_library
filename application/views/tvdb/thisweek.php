<section class="search_results">
<?php if(!empty($this->thisWeek)){
	echo '<ul class="media_list">';
	foreach($this->thisWeek['results'] as $results){ ?>

		<li>
		
			<a class="poster" href="<?=URL.'tvdb/tvinfo/'.$results['id']?>"><img src="<?=URL.PUBLIC_IMAGES.'btns/lazyload_poster.svg'?>" data-src="<?php echo $results['poster_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$results['poster_path'];?>" width="185px" height="278px"></a>
			<a class="title" href="<?=URL.'tvdb/tvinfo/'.$results['id']?>"><sub>Title: </sub><span><?=$results['name']?></span></a>
			<sub>Release Date: </sub><span class="date"><?=$results['first_air_date']?></span>

			<button class="add_show"></button>

			<form method="post" name="add_show" class="add_show" action="<?=URL.'tvdb/addtv'?>" >
				<input type="hidden" name="tv_id" value="<?=$results['id']?>">
				<input type="hidden" name="tv_title" value="<?=$results['name']?>">
				<input type="hidden" name="release_date" value="<?=$results['first_air_date']?>">
				<input type="hidden" name="poster_path" value="<?=$results['poster_path']?>">
				<button type="submit">Submit</button>
			</form>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>Try another search</p></div>";
}
$url = URL.'tvdb/thisWeek/'; 
$this->pagination($url, $this->thisWeek['total_pages']);
?>
</section>