<nav class="secondary_header">
	<ul>
		<li <?php if($this->checkForActiveControllerAndAction($filename, 'tvdb/tv')){echo 'class="active"';}?>
		><span><a href="<?=URL.'tvdb/tv'?>">My Shows</a></span></li>

		<li><span <?php if($this->mode == 'tvdb/thisweek'){echo 'class="active"';}?>
		><a href="<?=URL.'tvdb/thisweek'?>">On the Air</a></span></li>

		<li <?php if($this->mode == 'tvdb/today'){echo 'class="active"';}?>
		><span><a href="<?=URL.'tvdb/today'?>">Airing Today</a></span>
		</li>

		<li <?php if($this->mode == 'tvdb/popularShows'){echo 'class="active"';}?>
		><span><a href="<?=URL.'tvdb/popularShows'?>">Popular</a></span>
		</li>

	</ul>
</nav>