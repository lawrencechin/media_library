<?php
if(!empty($this->seasons)){
	echo '<ul class="episode_list">';
	foreach($this->seasons['episodes'] as $results){ ?>

		<li class="eps clearfix bottom_border_double">
			<div class="first">
				<img src="<?php echo $results['still_path'] == ''? URL.PUBLIC_IMAGES.'btns/default_poster.svg' : $this->imgURL.$results['still_path'];?>" width="300px" height="225px">
			</div>	
			<div class="second">
				<sub>Title: </sub><h2><?=$results['name']?></h2>
				<sub>Air Date: </sub><strong><?=$results['air_date']?></strong>
				<br>
				<hr>
				<sub>Episode <?=$results['episode_number']?>: </sub><span class="overview"><?=$results['overview']?></span>
			</div>

		</li>

	<?php }
	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>There was an error retrieving the episodes you wanted. Sorry.</p></div>";
}
?>