<?php

if(!empty($this->tvPosters)){
	$url = 'https://image.tmdb.org/t/p/';
	$thumb =  'w150';
	$original = 'original';
	echo '<ul class="media_list moviePosters">';

	foreach($this->tvPosters as $results){ ?>

		<li><a class="poster" href="<?=$url.$original.$results['file_path']?>"><img src="<?=$url.$thumb.$results['file_path']?>" width="150px" height="220px"></a></li>

	<?php }

	echo '</ul>';
}else{
	echo "<div class='no_results'><h2>No results</h2><p>No posters monsieur</p></div>";
}

?>