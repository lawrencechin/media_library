<?php
if(!empty($this->tvInfo)){
	echo '<section class="movieInfo" id="movieInfo" style="background-image: url(https://image.tmdb.org/t/p/w1280'.$this->tvInfo['backdrop_path'].')">'; ?>
	<button class="delete_modal" onclick="navigation.closeModal(this);"></button>

	<ul class="movieDetail" id="movieDetail">
		<li class="bottom_border_double"><h1><?=$this->tvInfo['name']?></h1></li>
		<li id="synopsis" class="justify paragraph-padding bottom_border_double"><sub>synopsis: </sub><span><?=$this->tvInfo['overview']?></span></li>

		<?php if(!empty($this->tvCredits)){ ?>
			<li id="cast_list" class="cast_list">
				<h2>Cast</h2>
				<ul>
				<?php foreach($this->tvCredits['cast'] as $cast){ ?>
					<li>
						<a class="contain_img" href="<?=URL.'tvdb/tvByPerson/'.$cast['id']?>"><img width="185px" src="<?=$cast['profile_path'] == ''? URL.PUBLIC_IMAGES.'btns/spacer.png' : $this->imgURL.$cast['profile_path']?>"></a>
						<sub>Character: </sub><span><?=$cast['character']?></span>
						<br>
						<sub>Actor: </sub><a href="<?=URL.'tvdb/tvByPerson/'.$cast['id']?>"><span><?=$cast['name']?></span></a>
					</li>
				<?php } ?> 
				</ul>
			</li>
		<?php }?>

		<li id="additional_content" class="additional_content"></li>
	</ul>

	<div class="horizontal_uls">
		<ul class="movie_additional" id="movie_additional">
			<li class="bottom_border"><sub>genre: </sub><span>
				<?php foreach($this->tvInfo['genres'] as $genres){
					echo $genres['name'] . ', ';
					}?>
			</span></li>
			<li class="bottom_border"><sub>release date: </sub><span><?=$this->tvInfo['first_air_date']?></span></li>
			<li><sub>seasons: </sub><span><?=$this->tvInfo['number_of_seasons']?></span></li>
			<li><sub>episodes: </sub><span><?=$this->tvInfo['number_of_episodes']?></span></li>
		</ul>

		<ul class="movie_options clearfix" id="movie_options">
			<li class="margin-right margin-top"><a href="<?=URL.'tvdb/images/'.$this->tvInfo['id']?>"><img src="<?=URL.'public/images/btns/picture2.svg'?>"><sub>view images</sub></a></li>
			<li class="margin-top"><a href="<?=URL.'tvdb/posters/'.$this->tvInfo['id']?>"><img src="<?=URL.'public/images/btns/texture.svg'?>"><sub>view posters</sub></a></li>
			<li class="margin-right"><a href="<?=URL.'tvdb/videos/'.$this->tvInfo['id']?>"><img src="<?=URL.'public/images/btns/videos.svg'?>"><sub>view videos</sub></a></li>
			<li><a href="<?=URL.'tvdb/similar/'.$this->tvInfo['id']?>"><img src="<?=URL.'public/images/btns/sites.svg'?>"><sub>similar shows</sub></a></li>
			<li id="synopsis_reveal" class="margin-right activeOption"><a href="#"><img src="<?=URL.'public/images/btns/learning.svg'?>"><sub>synopsis</sub></a></li>
			<li id="seasons_reveal"><a href="<?=URL.'tvdb/seasonList/'.$this->tvInfo['number_of_seasons'].'/'.$this->tvInfo['id']?>"><img src="<?=URL.'public/images/btns/stack.svg'?>"><sub>seasons</sub></a></li>
		</ul>
	</div>

	<div id="image_viewer" class="image_viewer">
		<header>
            <ul>
                <li id="previous"><button onclick="navigation.previous(this);" class="previous"></li>

                <li id="next"><button onclick="navigation.next(this);" class="next"></button></li>

                <li id="origSize" class="invert"><button onclick="navigation.origSize(this);" class="img_origSize"></button></li>

                <li id="conWidth"><button onclick="navigation.constrainWidth(this);" class="img_constrainWidth"></button></li>

                <li id="conHeight"><button onclick="navigation.constrainHeight(this);" class="img_constrainHeight"></button></li>

                <li id="closeModal"><button onclick="navigation.returnMovie(this);" class="closeModal"></button></li>
            </ul>
        </header>

        <div id="img_holder" class="img_holder">
        	<img src="<?=URL.'public/images/btns/img_loader.jpg'?>">
        </div>

        <div id="episodes" class="episodes">
        </div>
		
	</div>

<?php }else{ ?>
	<button class="delete_modal" onclick="navigation.closeModal(this);"></button> <?php

	echo "<div class='no_results'><h2>No results</h2><p>There was an error retrieving the show you wanted. Sorry.</p></div>";
}
?>
</section>
<script>
navigation.movieOptions('#movie_options');
navigation.imageDisplay('#additional_content');
</script>