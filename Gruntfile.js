module.exports = function(grunt){
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			options: {
				mangle: false,
				preserveComments: false,
				compress : true
			},
				
			compile: {
				files: {
			        'public/js/min/script.min.js': 'public/js/fragments/general.js'
			      }
			}
		},

		cssmin: {
			options: {
		    	shorthandCompacting: false,
		    	roundingPrecision: -1
			},
			target: {
			    files: {
			       'public/css/screen.min.css' : 'public/css/screen-prefixed.css'
			    }
		  	}
		},

		sass: {
			dist: {
				options: {
					style : 'compact',
					trace : true
				},

				files:[{
					expand: true,
					cwd: 'public/css/sass/',
					src: '*.scss',
					dest: 'public/css/',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			dist: {
				files: {
					'public/css/screen-prefixed.css' : 'public/css/screen.css'
				}
			}
		},

		watch: {
            sass: {
                files: ['public/css/sass/*.scss'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },

            uglify: {
            	files: ['public/js/fragments/general.js'],
            	tasks: ['uglify', 'compile']
            },

            options: {
            	spawn : false,
            	event : 'changed'
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	//Default task(s)
	grunt.registerTask('compile', 'uglify');
};