var utils = {
	
	loading : $('#loading_and_thumbs'),
	snp : $('#shallNotPass'),
	url : 'http://medialibrary.000space.com/',

	keyBoardControls: function(){
		var omnibar = $('#search_all'),
		changeContext = function(id){
			$(id).find('a').trigger('click');
			$(id).find('a').trigger('click');
			setTimeout(function(){
				omnibar.val('');
			}, 300);
		};

		Mousetrap.bind('! s a', function(){ 
			changeContext('#sa');
		});

		Mousetrap.bind('! m m', function(){ 
			changeContext('#smm');
		});

		Mousetrap.bind('! m s', function(){ 
			changeContext('#sms');
		});

		Mousetrap.bind('x', function(){
			if($('#closeModal').is(':visible')){
				$('#closeModal').find('button').trigger('click');
			}
		});

		Mousetrap.bind('return', function(){
			$('#submit_search_all').trigger('click');
		});
	}, 

	pleaseWait : function(){
		utils.loading.fadeIn();
		utils.snp.addClass('loading');
	},

	simpleSuccess : function(){
		utils.loading.fadeOut();
	},

	success : function(){
		utils.snp.removeClass('loading').addClass('success tada');
		utils.loading.find('p').html('Success!');
		
		setTimeout(function(){
			utils.loading.fadeOut();
			utils.snp.removeClass('success tada');
			utils.loading.find('p').html('Please Wait');
		}, 800);
	},

	error : function(resp){

		utils.snp.removeClass('loading').addClass('error wobble');
		utils.loading.find('p').html(resp.msg);
		
		setTimeout(function(){
			utils.loading.fadeOut();
			utils.snp.removeClass('error wobble');
			utils.loading.find('p').html('Please Wait');
		}, 2000);
	}
};

var navigation = {

	clickedLink : null,

	load : function(cont){
		$(cont).on('click', "a:not('.noModal')", function(e){
			e.preventDefault();
			var $this = $(this),
			$link = $this.attr('href'),
			$modal = $('<div id="modal"></div>'),
			$currentModal = $('#modal');

			if($currentModal.length){
				$currentModal.fadeOut();

				setTimeout(function(){
					$currentModal.remove();
				}, 800);
			}

			utils.pleaseWait();

			$.ajax({
				url : $link,
				dataType : 'html'
			}).done(function(resp){
				utils.success();
				$modal.html(resp);

				$('body').append($modal);
				$modal.fadeIn();
				document.body.style.height = window.innerHeight + 'px';
				document.body.style.overflow = 'hidden';	

			}).error(function(resp){
				utils.error(resp);
			});
			
		});
	},

	closeModal : function(btn){
		var $btn = $(btn);
		$btn.parents('#modal').fadeOut();
		$('body').removeAttr("style");
		setTimeout(function(){
			$btn.parents('#modal').remove();
		}, 600);
	},

	movieOptions : function(cont){
		$(cont).on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			$link = $this.attr('href'),
			$movAdd = $('#additional_content'),
			$movOpt = $('#movie_options'),
			$synopsis = $('#synopsis'),
			$cast_list = $('#cast_list');

			$movOpt.find('.activeOption').removeClass('activeOption');
			$this.parent().addClass('activeOption');

			if($this.parent().attr('id') === 'synopsis_reveal'){
				$synopsis.fadeIn();
				$cast_list.fadeIn();
				$movAdd.empty();
			}else{
				$synopsis.fadeOut();
				$cast_list.fadeOut();
				$movAdd.empty();
				$movAdd.load($link);
			}
		});
	}, 

	moreBio : function(btn){
		var $btn = $(btn),
		$cont = $btn.siblings('div.bio');

		if($btn.attr('data-more') == 1){
			$btn.attr('data-more', 0).text('Less');
			$cont.css({
				'height' : 'auto',
				'overflow' : 'auto'
			});
		}else{
			$btn.attr('data-more', 1).text('More');
			$cont.css({
				'height' : '',
				'overflow' : ''
			});
		}
	},

	imageDisplay : function(cont){
		var $cont = $(cont);

		$cont.on('click', 'a:not(".externalLinks")', function(e){
			e.preventDefault();
			var $this = $(this),
			$link = $this.attr('href'),
			$imgView = $('#image_viewer'),
			$img = $imgView.find('img'),
			$imgViewDiv = $imgView.find('#img_holder'),
			$episodes = $imgView.find('#episodes');

			utils.pleaseWait();

			navigation.clickedLink = $this.parent();
			//if a season link is clicked
			if($this.hasClass('seasons')){
				$episodes.load($link, function(){
					utils.simpleSuccess();
					$('#origSize').hide();
					$('#conWidth').hide();
					$('#conHeight').hide();
					$imgViewDiv.hide();
					$episodes.fadeIn();
				});
			}else{
				//load image in background and display when loaded
				$('<img/>').attr('src', $link).load(function() {
					$(this).remove(); // prevent memory leaks as @benweet suggested
					$('#origSize').show();
					$('#conWidth').show();
					$('#conHeight').show();
					$img.attr('src', $link);
					utils.simpleSuccess();
					$imgViewDiv.scrollsync();
					$imgViewDiv.dragscrollable();
					$episodes.hide().empty();
					$imgViewDiv.fadeIn();
				});

				setTimeout(function(){
					utils.simpleSuccess();
				}, 10000);
			}
			

			$imgView.addClass('showImage');
		});
	},

	returnMovie : function(btn){
		$(btn).parents('#image_viewer').removeClass('showImage');
		$('#episodes').empty();
	},

	origSize : function(btn){
		$(btn).parents('ul').find('.invert').removeClass('invert').end().end().parent().addClass('invert');
		$('#image_viewer').find('img').get(0).removeAttribute('style');
	},

	constrainWidth : function(btn){
		$(btn).parents('ul').find('.invert').removeClass('invert').end().end().parent().addClass('invert');
		$('#image_viewer').find('img').get(0).removeAttribute('style');
		$('#image_viewer').find('img').get(0).style.width = '100%';
	},

	constrainHeight : function(btn){
		$(btn).parents('ul').find('.invert').removeClass('invert').end().end().parent().addClass('invert');
		$('#image_viewer').find('img').get(0).removeAttribute('style');
		$('#image_viewer').find('img').get(0).style.height = '100%';
	},

	previous : function(){
		var prevElem = this.clickedLink.prev();
		if(prevElem.length > 0){
			prevElem.find('a').first().trigger('click');
		}else{
			this.clickedLink.parent().find('li > a').last().trigger('click');
		}
	},

	next : function(){
		var nextElem = this.clickedLink.next();
		if(nextElem.length > 0){
			nextElem.find('a').first().trigger('click');
		}else{
			this.clickedLink.parent().find('li > a').first().trigger('click');
		}
	},
};

var forms = {

	$pressedBtn : null,
	$activeForm : null,

	formBtn : function($cont){
		$cont.on('click', 'button', function(e){
			e.preventDefault();
			var $this = $(this),
			$classBtn = $this.attr('class').split(' ')[0],
			$form = $this.siblings('form.'+$classBtn);

			forms.$pressedBtn = $this;
			forms.$activeForm = $form;
			if(!e){ e = window.event; }
			forms.submitForm($form, e);
		});
	},

	submitForm : function($form, e){
		e.preventDefault();
		var $action = $form.attr('action'),
		$data = new FormData($form.get(0));
		forms.ajaxSend($action, $data);
	},

	ajaxSend : function(action, data){

		utils.pleaseWait();

		$.ajax({
			url : action,
			data : data,
			type : 'POST',
			dataType : 'json',
			processData: false,
			contentType: false
		})

		.done(function(resp){
			console.log(resp);
			forms.ajaxResult(resp);
		})

		.error(function(resp){
			console.log(resp);
			utils.error(resp);
		});
	},

	ajaxResult : function($json){
		//utils.simpleSuccess();
		//utils.success();
		//utils.error($json);
		switch($json.statusCode){
			//mark movie/tv favourite
			case '01' :
			case '11' :
				utils.simpleSuccess();
				forms.$pressedBtn.addClass('activeFavOrWatched');
				forms.$activeForm.find('input[name="favourite"]').val(0);
				break;
			//remove movie/tv from favourites	
			case '02' : 
			case '12' : 
				utils.simpleSuccess();
				forms.$pressedBtn.removeClass('activeFavOrWatched');
				forms.$activeForm.find('input[name="favourite"]').val(1);
				break;
			//mark movie/tv watched	
			case '03' :
			case '13' :
				utils.simpleSuccess();
				forms.$pressedBtn.addClass('activeFavOrWatched');
				forms.$activeForm.find('input[name="watched"]').val(0);
				break;
			//mark movie/tv unwatched
			case '04' :
			case '14' :  
				utils.simpleSuccess();
				forms.$pressedBtn.removeClass('activeFavOrWatched');
				forms.$activeForm.find('input[name="watched"]').val(1);
				break;
			case '05' :
			case '15' :
				utils.simpleSuccess();
				forms.$pressedBtn.parents('li').addClass('removeElem');
				setTimeout(function(){
					forms.$pressedBtn.parents('li').remove();
				},800);
				break;
			case '06' :
			case '16' :
			case '99' :
				utils.error($json);
				break;
			case '07' :
			case '17' :
				utils.error($json);
				break;

			default :
				utils.success();
				break;								
		}
	}
};

var sort = {
	sortBy : function(cont){
		var $cont = $(cont);

		$cont.on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this);
			$cont.toggleClass('cascade');
			$cont.children('li').removeClass('search_active');
			$this.parent().addClass('search_active');
		});
	},

	submitSort : function(btn){
		var $btn = $(btn),
		$optionOne = $btn.siblings('.firstSortingCriteria').find('.search_active').children('a').attr('href'),
		$optionTwo = $btn.siblings('.secondSortingCriteria').find('.search_active').children('a').attr('href'),
		$url = $optionOne + $optionTwo;
		document.location.href = $url;
	}
};

var search = {

	autoComplete : null,

	omniBar : function(){
		var $container = $('.search_all'),
		$input = $container.find('#search_all'),
		$search_options = $container.find('.search_options');

		$.getJSON(utils.url+'moviedb/autocomplete', function(response){
			$input.autocomplete({
				lookup : response,
			});

			
		});

		$search_options.on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this);
			$search_options.toggleClass('cascade');
			$search_options.children('li').removeClass('search_active');
			$this.parent().addClass('search_active');
		});
	},

	submit_search : function(btn){
		var $btn = $(btn),
		$searchOpt = $('.search_options').find('.search_active'), 
		$input = $btn.siblings('input'),
		$inputVal = encodeURIComponent($input.val()).replace(/%20/g,'_'),
		$link = $searchOpt.find('a').attr('href'),
		$url = $link + $inputVal;
		document.location.href = $url;
	}
};

var feeds = {
	feedURL : "http://screenrant.com/feed",
	maxCount : 12,
	containerFragment : '#news_',

	getFeeds : function(){
		$.ajax({
			url : 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=' + feeds.maxCount + '&output=json&q=' + encodeURIComponent(feeds.feedURL) + '&hl=en&callback=?',
			dataType : 'json'
		}).success(function(data){
			if(!$.isEmptyObject(data.responseData)){
				feeds.insertContent(data);
			}else{
				feeds.removeNews();
			}
			
		}).error(function(){
			feeds.removeNews();
		});
	},

	insertContent : function(data){

		for(var i = 0; i < feeds.maxCount; i++){
			var $container = $(feeds.containerFragment+i),
			$dateCut = data.responseData.feed.entries[i].publishedDate.substring(0, data.responseData.feed.entries[i].publishedDate.indexOf(':') - 2),
			$date = '<span class="date"><sub>'+$dateCut+'</sub></span>',
			$img = $(data.responseData.feed.entries[i].content).find('img')[0],
			$title = '<p class="news_title"><a class="noModal" href="'+data.responseData.feed.entries[i].link+'" target="_blank">'+data.responseData.feed.entries[i].title+'</a></p>';
			//$snippet = '<p class="news_snippet">'+data.responseData.feed.entries[i].contentSnippet+'</p>';
			$container.append($img, $title, $date);
		}
	},

	removeNews : function(){
		for(var i = 0; i < 12; i++){
			$(feeds.containerFragment+i).remove();
		}
	}

};

/*
 * jQuery.liveFilter
 * Copyright (c) 2009 Mike Merritt
 * Forked by Lim Chee Aun (cheeaun.com)
 * 
 */
(function($){
	$.fn.liveFilter = function(inputEl, filterEl, options){
		var defaults = {
			filterChildSelector: null,
			filter: function(el, val){
				return $(el).text().toUpperCase().indexOf(val.toUpperCase()) >= 0;
			},
			before: function(){},
			after: function(){}
		};
		var options = $.extend(defaults, options);
		
		var el = $(this).find(filterEl);
		if (options.filterChildSelector) el = el.find(options.filterChildSelector);

		var filter = options.filter;
		$(inputEl).keyup(function(){
			var val = $(this).val();
			var contains = el.filter(function(){
				return filter(this, val);
			});
			var containsNot = el.not(contains);
			if (options.filterChildSelector){
				contains = contains.parents(filterEl);
				containsNot = containsNot.parents(filterEl).hide();
			}
			
			options.before.call(this, contains, containsNot);
			
			contains.show();
			containsNot.hide();
			
			if (val === '') {
				contains.show();
				containsNot.show();
			}
			
			options.after.call(this, contains, containsNot);
		});
	}
})(jQuery);

/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);

/*
 * jQuery dragscrollable Plugin
 * version: 1.0 (25-Jun-2009)
 * Copyright (c) 2009 Miquel Herrera
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */
;(function($){ // secure $ jQuery alias


$.fn.dragscrollable = function( options ){
   
	var settings = $.extend({   
		dragSelector:'>:first',
		acceptPropagatedEvent: true,
        preventDefault: true
	},options || {});
	 
	
	var dragscroll= {
		mouseDownHandler : function(event){
			// mousedown, left click, check propagation
			if (event.which!=1 ||
				(!event.data.acceptPropagatedEvent && event.target != this)){ 
				return false; 
			}
			
			// Initial coordinates will be the last when dragging
			event.data.lastCoord = {left: event.clientX, top: event.clientY}; 
		
			$.event.add( document, "mouseup", 
						 dragscroll.mouseUpHandler, event.data );
			$.event.add( document, "mousemove", 
						 dragscroll.mouseMoveHandler, event.data );
			if (event.data.preventDefault) {
                event.preventDefault();
                return false;
            }
		},

		mouseMoveHandler : function(event) { // User is dragging
			// How much did the mouse move?
			var delta = {left: (event.clientX - event.data.lastCoord.left),
						 top: (event.clientY - event.data.lastCoord.top)};
			
			// Set the scroll position relative to what ever the scroll is now
			event.data.scrollable.scrollLeft(
							event.data.scrollable.scrollLeft() - delta.left);
			event.data.scrollable.scrollTop(
							event.data.scrollable.scrollTop() - delta.top);
			
			// Save where the cursor is
			event.data.lastCoord={left: event.clientX, top: event.clientY};
			if (event.data.preventDefault) {
                event.preventDefault();
                return false;
            }

		},
		mouseUpHandler : function(event) { // Stop scrolling
			$.event.remove( document, "mousemove", dragscroll.mouseMoveHandler);
			$.event.remove( document, "mouseup", dragscroll.mouseUpHandler);
			if (event.data.preventDefault) {
                event.preventDefault();
                return false;
            }
		}
	};
	
	// set up the initial events
	this.each(function() {
		// closure object data for each scrollable element
		var data = {scrollable : $(this),
					acceptPropagatedEvent : settings.acceptPropagatedEvent,
                    preventDefault : settings.preventDefault };
		// Set mouse initiating event on the desired descendant
		$(this).find(settings.dragSelector).
						bind('mousedown', data, dragscroll.mouseDownHandler);
	});
}; //end plugin dragscrollable

})( jQuery ); // confine scope

/*
 * jQuery scrollsync Plugin
 * version: 1.0 (30 -Jun-2009)
 * Copyright (c) 2009 Miquel Herrera
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */
;(function($){ // secure $ jQuery alias


$.fn.scrollsync = function( options ){
	var settings = $.extend(
			{   
				targetSelector:':first',
				axis: 'xy'
			},options || {});
	
	
	function scrollHandler(event) {
		if (event.data.xaxis){
			event.data.followers.scrollLeft(event.data.target.scrollLeft());
		}
		if (event.data.yaxis){
			event.data.followers.scrollTop(event.data.target.scrollTop());
		}
	}
	
	// Find target to follow and separate from followers
	settings.target = this.filter(settings.targetSelector).filter(':first');
	settings.followers=this.not(settings.target); // the rest of elements

	// Parse axis
	settings.xaxis= (settings.axis=='xy' || settings.axis=='x') ? true : false; 
	settings.yaxis= (settings.axis=='xy' || settings.axis=='y') ? true : false;
	if (!settings.xaxis && !settings.yaxis) return;  // No axis left 
	
	// bind scroll event passing array of followers
	settings.target.bind('scroll', settings, scrollHandler);
	
}; // end plugin scrollsync

})( jQuery ); // confine scope
