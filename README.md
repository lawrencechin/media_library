# Media Library

![media library](medialibrary.jpg)

After a hard drive crash took out my whole collection of neatly tagged media I decided to take a backup **iTunes** library file and create a cataloguing website. The title is misnomer: the purpose of the site to maintain a collection of films & television shows (sans any actual media files) eschewing music, books, podcasts etc… 

The site is written in **PHP** and uses API's provided by [The Movie DB](https://www.themoviedb.org). 
