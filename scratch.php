Array
(
    [air_date] => 2014-08-13
    [episodes] => Array
        (
            [0] => Array
                (
                    [air_date] => 2014-08-13
                    [episode_number] => 1
                    [id] => 1003800
                    [name] => Pilot
                    [overview] => Martin Odum has the uncanny ability to transform himself into a completely different person for each job. But he begins to question his own identity when a mysterious stranger suggests that Martin isn't the man he believes himself to be.
                    [still_path] => /l0XYveXwnZ8x8YN344SYAVtBmw0.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [1] => Array
                (
                    [air_date] => 2014-08-20
                    [episode_number] => 2
                    [id] => 1003801
                    [name] => Chemistry
                    [overview] => Martin, Crystal and Troy Buchannan go undercover when a chemistry teacher and his family are kidnapped. The case takes on urgency when it's feared the kidnappers will use the teacher to manufacture a deadly gas.
                    [still_path] => /j92giq1vi6HbMExL21b1IgU6MlV.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [2] => Array
                (
                    [air_date] => 2014-08-27
                    [episode_number] => 3
                    [id] => 1003802
                    [name] => Lords of War
                    [overview] => Martin poses as an international arms dealer to rescue kidnapped chemist Richard Hubbard and track the sale of VX gas. Before long, he connects with a beautiful arms trader and slips into a shadowy romance with her. 
                    [still_path] => /yYhuZ9TLp90c8vHS9gnW7CwsZqt.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [3] => Array
                (
                    [air_date] => 2014-09-03
                    [episode_number] => 4
                    [id] => 1003803
                    [name] => Betrayal
                    [overview] => 
                    [still_path] => /8lAGoiyoCWdLyUogK8bDhTjLLiJ.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [4] => Array
                (
                    [air_date] => 2014-09-10
                    [episode_number] => 5
                    [id] => 1003804
                    [name] => Rogue
                    [overview] => 
                    [still_path] => /wo7N7zh80hwNEgWkBLbw3u5J5uE.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [5] => Array
                (
                    [air_date] => 2014-09-17
                    [episode_number] => 6
                    [id] => 1003805
                    [name] => Gauntlet
                    [overview] => 
                    [still_path] => /h4LbgIcv8BHDLrtsQ4HBe1oM03P.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [6] => Array
                (
                    [air_date] => 2014-09-24
                    [episode_number] => 7
                    [id] => 1003806
                    [name] => Quicksand
                    [overview] => 
                    [still_path] => /vViVjuW4XdyMfTplYYVtV9P9Phw.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [7] => Array
                (
                    [air_date] => 2014-10-01
                    [episode_number] => 8
                    [id] => 1003807
                    [name] => Iconoclast
                    [overview] => 
                    [still_path] => /jvEAGCL1NsbjilqYvwNelSieNMQ.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [8] => Array
                (
                    [air_date] => 2014-10-08
                    [episode_number] => 9
                    [id] => 1003808
                    [name] => Wilderness of Mirrors
                    [overview] => 
                    [still_path] => /11c2Gr9iCNQVbOoPONzQQB8H7E4.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

            [9] => Array
                (
                    [air_date] => 2014-10-15
                    [episode_number] => 10
                    [id] => 1003809
                    [name] => Identity
                    [overview] => 
                    [still_path] => /njMKxdbopQcHPFR6Dsc1mbw6trZ.jpg
                    [vote_average] => 0
                    [vote_count] => 0
                )

        )

    [name] => Season 1
    [overview] => Based on the award-winning book by master spy novelist Robert Littell, Follows a deep-cover operative named Martin Odum, who has an uncanny ability to transform himself into a different person for each job. But his own identity comes into question when a mysterious stranger suggests that Martin isn't who he thinks he is.
    [id] => 60912
    [poster_path] => /qMYFIxAM2bAiNORSOB5kMkSOQFJ.jpg
    [season_number] => 1
)